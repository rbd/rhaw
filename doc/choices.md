Why these choices?

This file should describe most choices that have been made in the design of Rhaw,
and explain those.

Bootloader
==========

A very basic 2 stages bootloader has been written in plain x86 assembly.
Since writting a bootloader is very difficult and hardware dependent,
the objective always has been to *learn* how bootloaders are made,
and certainly not to provide a realistic one.

That is why you will find a bootloader here,
but the project uses Grub.

