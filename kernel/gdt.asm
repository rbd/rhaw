section .data
global gdt_pointer:data
gdt_start:
;; flat memory model
;; kernel space and user space all have the entire memory, just different rings.
;; 0x00 null
;; 0x08 kernel code
;; 0x10 kernel data
;; 0x18 user code
;; 0x20 user data
    .null:
        dq 0
    .kernel_code:
        ;; base: 0, limit: 4G
        ;; ring 0, executable
        dw 0xFFFF ; limit [0:15]
        dw 0 ; base [0:15]
        db 0 ; base [16:23]
        ; access byte
        ; (present | ring 00b | code/data | +x | restrict access to ring 0 | readable | accessed bit)
        db 10011010b
        db 0xCF ; limit [16:19] and flags (1100 = 32 bits | 4K page granularity)
        db 0x0 ; base [24:31]
    .kernel_data:
        ;; base: 0, limit: 4G
        ;; ring 0, read-write
        dw 0xFFFF ; limit [0:15]
        dw 0 ; base [0:15]
        db 0 ; base [16:23]
        ; access byte
        ; (present | ring 00b | code/data | -x | direction | writable | accessed bit)
        db 10010010b
        db 0xCF ; limit [16:19] and flags (1100 = 32 bits | 4K page granularity)
        db 0x0 ; base [24:31]
    .user_code:
        ;; base: 0, limit: 4G
        ;; ring 0, read-only
        dw 0xFFFF ; limit [0:15]
        dw 0 ; base [0:15]
        db 0 ; base [16:23]
        ; access byte
        ; (present | ring 11b | code/data | +x | don’t care about access | readable | accessed bit)
        db 11111010b
        db 0xCF ; limit [16:19] and flags (1100 = 32 bits | 4K page granularity)
        db 0x0 ; base [24:31]
    .user_data:
        ;; base: 0, limit: 4G
        ;; ring 0, read-write
        dw 0xFFFF ; limit [0:15]
        dw 0 ; base [0:15]
        db 0 ; base [16:23]
        ; access byte
        ; (present | ring 11b | code/data | -x | direction | writable | accessed bit)
        db 11110010b
        db 0xCF ; limit [16:19] and flags (1100 = 32 bits | 4K page granularity)
        db 0x0 ; base [24:31]
gdt_end:
gdt_pointer:
    dw gdt_end - gdt_start - 1
    dd gdt_start

