global pic_initialize

section .text
;; input: none
;; output: none
;; registers scrapped: eax, ebx
pic_initialize:
    ;; save masks
    in al, 0x21
    shl eax, 8
    in al, 0xA1
    mov ebx, eax

    ;; ICW1:
    ;; bit 0 set means we will send a ICW4
    ;; bit 4 set means this is ICW1
    mov al, 0x11
    out 0x20, al
    out 0xA0, al

    ;; ICW2
    ;; map primary PIC’s IQ0–IQ7 to 32–39
    ;; map secondary PIC’s IQ0–IQ7 to 40–47
    ;; (these mappings are pretty standard; see the intel x86 manual chap 6)
    mov al, 32
    out 0x21, al
    mov al, 40
    out 0xA1, al

    ;; ICW3
    ;; specify that the primary PIC’s IQ2 pin is linked to the secondary PIC’s
    ;; INT pin
    mov al, 00000100b ; the only set is the n°2
    out 0x21, al
    mov al, 2 ; the secondary pic simply waits for a number
    out 0xA1, al

    ;; ICW4
    ;; bit 0 set means we’re in a 80x86 architecture (i.e. 16, 32 or 64 bits)
    mov al, 1
    out 0x21, al
    out 0xA1, al

    ;; restore masks
    mov eax, ebx
    out 0xA1, al
    shr eax, 8
    out 0x21, al
