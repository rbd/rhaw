#include <stdint.h>

int
isprint(uint8_t c) {
    return c >= 0x20 && c <= 0x7E;
}
