extern divide_by_zero_handler
extern debug_trap_handler
extern non_maskable_interrupt_handler
extern breakpoint_handler
extern overflow_handler
extern bound_range_exceeded_handler
extern invalid_opcode_handler
extern device_not_available_handler
extern double_fault_handler
extern invalid_tss_handler
extern segment_not_present_handler
extern stack_segment_fault_handler
extern general_protection_fault_handler
extern page_fault_handler
extern iq0_handler

global idt_init:function, idt_pointer:data

section .text
;; exception handlers locations aren’t known at compile time,
;; only after linking; thus, we setup the idt at runtime.
;; registers scrapped: eax, ebx
idt_init:
    .divide_by_zero:
        lea eax, [idt_start+0*8]
        lea ebx, [divide_by_zero_wrapper]
        call set_interrupt_descriptor
    .debug_trap:
        lea eax, [idt_start+1*8]
        lea ebx, [debug_trap_wrapper]
        call set_interrupt_descriptor
    .non_maskable_interrupt:
        lea eax, [idt_start+2*8]
        lea ebx, [non_maskable_interrupt_wrapper]
        call set_interrupt_descriptor
    .breakpoint:
        lea eax, [idt_start+3*8]
        lea ebx, [breakpoint_wrapper]
        call set_interrupt_descriptor
    .overflow:
        lea eax, [idt_start+4*8]
        lea ebx, [overflow_wrapper]
        call set_interrupt_descriptor
    .bound_range_exceeded:
        lea eax, [idt_start+5*8]
        lea ebx, [bound_range_exceeded_wrapper]
        call set_interrupt_descriptor
    .invalid_opcode:
        lea eax, [idt_start+6*8]
        lea ebx, [invalid_opcode_wrapper]
        call set_interrupt_descriptor
    .device_not_available:
        lea eax, [idt_start+7*8]
        lea ebx, [device_not_available_wrapper]
        call set_interrupt_descriptor
    .double_fault:
        lea eax, [idt_start+8*8]
        lea ebx, [double_fault_wrapper]
        call set_interrupt_descriptor
    ;; skip descriptor 9 (legacy, not used anymore)
    .invalid_tss:
        lea eax, [idt_start+10*8]
        lea ebx, [invalid_tss_wrapper]
        call set_interrupt_descriptor
    .segment_not_present:
        lea eax, [idt_start+11*8]
        lea ebx, [segment_not_present_wrapper]
        call set_interrupt_descriptor
    .stack_segment_fault:
        lea eax, [idt_start+12*8]
        lea ebx, [stack_segment_fault_wrapper]
        call set_interrupt_descriptor
    .general_protection_fault:
        lea eax, [idt_start+13*8]
        lea ebx, [general_protection_fault_wrapper]
        call set_interrupt_descriptor
    .page_fault:
        lea eax, [idt_start+14*8]
        lea ebx, [page_fault_wrapper]
        call set_interrupt_descriptor

    ;; skip every other exception -> handled as a GPF for now

    ;; hardware interrupts
    .iq0:
        lea eax, [idt_start+32*8]
        lea ebx, [iq0_wrapper]
        call set_interrupt_descriptor

    ret

;; input:
;;     eax: pointer to the idt entry
;;     ebx: pointer to the interrupt handler
set_interrupt_descriptor:
    mov [eax], bx
    mov [eax+2], word 0x08
    mov [eax+4], byte 0
    mov [eax+5], byte 10001110b
    shr ebx, 16
    mov [eax+6], bx
    ret

;; Our exception handlers are written in C, but cannot be called before
;; we save the current state of affairs.
;; Since it’s always the same thing, let’s put it in a macro.
%macro exception_wrapper 1
    ;; save registers that could be scrapped by a C function call
    push eax
    push ecx
    push edx
    call %1
    ;; restore everything
    pop edx
    pop ecx
    pop eax
    iret
%endmacro

;; Some exceptions push a useful code to the stack, code that we need to
;; feed our C handlers with.
%macro exception_wrapper_with_code 1
    ;; retrieve the error code (and save ebx, since we use it)
    xchg [esp], ebx
    ;; save registers
    push eax
    push ecx
    push edx
    ;; push the error code
    push ebx
    call %1
    ;; restore everything
    add esp, 4
    pop edx
    pop ecx
    pop eax
    pop ebx
    iret
%endmacro

;; Hardware interrupts needs us to send a End Of Interrupt command to the PIC
;; once we’ve finished our business.
%macro hardware_interrupt_wrapper 1
    ;; save registers that could be scrapped by a C function call
    push eax
    push ecx
    push edx
    call %1
    ;; send EOI
    mov eax, 0x20
    out 0x20, al
    ;; restore everything
    pop edx
    pop ecx
    pop eax
    iret
%endmacro

divide_by_zero_wrapper:
    exception_wrapper divide_by_zero_handler

debug_trap_wrapper:
    exception_wrapper debug_trap_handler

non_maskable_interrupt_wrapper:
    exception_wrapper non_maskable_interrupt_handler

breakpoint_wrapper:
    exception_wrapper breakpoint_handler

overflow_wrapper:
    exception_wrapper overflow_handler

bound_range_exceeded_wrapper:
    exception_wrapper bound_range_exceeded_handler

invalid_opcode_wrapper:
    exception_wrapper invalid_opcode_handler

device_not_available_wrapper:
    exception_wrapper device_not_available_handler

double_fault_wrapper:
    exception_wrapper_with_code double_fault_handler

invalid_tss_wrapper:
    exception_wrapper_with_code invalid_tss_handler

segment_not_present_wrapper:
    exception_wrapper_with_code segment_not_present_handler

general_protection_fault_wrapper:
    exception_wrapper_with_code general_protection_fault_handler

stack_segment_fault_wrapper:
    exception_wrapper_with_code stack_segment_fault_handler

page_fault_wrapper:
    exception_wrapper_with_code page_fault_handler


iq0_wrapper:
    hardware_interrupt_wrapper iq0_handler

section .data
idt_start:
    times 256 dq 0
idt_end:
idt_pointer:
    dw idt_end - idt_start - 1
    dd idt_start



