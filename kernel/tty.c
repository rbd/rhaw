#include <u.h>
#include <libc.h>
#include <tty.h>

/* ******************** VGA ******************** */

static uint16_t *vga_buf = (uint16_t *)0xB8000;

enum {
    N_LINES = 25,
    N_COLUMNS = 80,
    N_CELLS = 25*80,
};

enum {
    VGA_COLOR_CLEAR = 0,
    VGA_COLOR_TEXT = 0x0F,
};

#define vga_cell(attrs, c) (((attrs) << 8) + (c))

/**
 * Clear screen
 */
static void
vga_clear(void) {
    uint16_t *p = vga_buf;

    for (; p - vga_buf < N_CELLS; ++p) {
        *p = VGA_COLOR_CLEAR;
    }
}

static void
vga_scroll_up(void) {
    uint16_t *p;

    memmove(vga_buf, vga_buf + N_COLUMNS, (N_CELLS-N_COLUMNS)*sizeof(*vga_buf));
    for (p = vga_buf + (N_CELLS-N_COLUMNS);
            p - vga_buf < N_CELLS; ++p) *p = 0;
}

/* ******************** TTY ******************** */

static uint16_t cursor;

enum {
    TAB_WIDTH = 4,
};

void
tty_reset(void) {
    vga_clear();
    cursor = 0;
}

void
tty_putc(uint8_t c) {
    switch (c) {
        case '\0':
            break;
        case '\t':
            /* OPTIMISABLE */
            cursor = (cursor/TAB_WIDTH)*TAB_WIDTH + TAB_WIDTH;
            break;
        case '\n':
            cursor += N_COLUMNS;
            if (cursor > N_CELLS) {
                vga_scroll_up();
                cursor = N_CELLS-N_COLUMNS;
            }
            break;
        case '\r':
            cursor = (cursor/N_COLUMNS)*N_COLUMNS;
            break;
        default:
            if (isprint(c)) {
                vga_buf[cursor++] = vga_cell(VGA_COLOR_TEXT, c);
            }
            break;
    }
}

void
tty_puts(const char *s) {
    while (*s) tty_putc(*s++);
}

