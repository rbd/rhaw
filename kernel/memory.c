#include <u.h>
#include <libc.h>
#include <multiboot.h>
#include <memory.h>

#define LENGTH(t) (sizeof(t)/sizeof(*t))
#define FOREACH_HUNK(i, q) \
    for (i = q->first; \
            i != (q->first+q->length) % LENGTH(q->buffer); \
            i = (i+1) % LENGTH(q->buffer))


/******************** Physical memory ********************/

/* Return a pointer to the element in front of q,
 * or NULL if q is empty.  */
static struct mem_hunk *
peek(struct mem_queue *q) {
    return q->length ? &q->buffer[q->first] : NULL;
}

/* Push an element at the end of q.
 * Return the length of the queue if it is ok,
 * -1 if there’s no more room in our queue. */
static int
push(struct mem_queue *q, page_frame_t *addr, size_t size) {
    const size_t i = (q->first+q->length) % LENGTH(q->buffer);

    if (q->length == LENGTH(q->buffer)) return -1;
    q->buffer[i].addr = addr;
    q->buffer[i].size = size;
    return ++(q->length);
}

/* Remove the element in front of q. */
static void
pop(struct mem_queue *q) {
    if (q->length) q->first = (q->first+1) % LENGTH(q->buffer);
}

/* Initialize the physical memory allocator.
 * Return 0 if it is ok, a negative value if something went wrong. */
int
pmem_init(struct mem_queue *q, const struct multiboot_info *info) {
    struct multiboot_mmap_entry *mmap = 0;
    uintptr_t base = 0;
    int ret = 0;

    if (!(info->flags & MB_INFO_VALID_MEMFIELDS)) {
        return -1;
    }
    if (!(info->flags & MB_INFO_VALID_MMAP)) {
        return -2;
    }

    q->first = 0;
    q->length = 0;

    FOREACH_MMAP(mmap, info) {
        if ((unsigned) mmap->type == MB_MMAP_ENTRY_AVAILABLE) {
            base = (uintptr_t) mmap->base_addr;
            /* correctly align our memory base */
            base = (base/PAGE_SIZE + !!(base % PAGE_SIZE)) * PAGE_SIZE;
            ret = push(q, (page_frame_t *) base, (size_t) mmap->length);
            if (ret < 0) return ret;
        }
    }
    return 0;
}

/* Allocate exactly one page frame.
 * Return NULL if there’s no more memory. */
void *
pmem_alloc(struct mem_queue *q) {
    struct mem_hunk *hunk = peek(q);
    void *addr = 0;

    if (!hunk) return NULL; /* no more memory */
    hunk->size--;
    addr = hunk->addr;
    if (hunk->size) hunk->addr++; /* hunk points to the next page */
    else pop(q); /* this hunk is exhausted, forget it */
    return addr;
}

/* Free exactly one page frame.
 * Return a negative number if there’s no more room in our allocator’s page:
 * you might want to reduce memory fragmentation and try again. */
int
pmem_free(struct mem_queue *q, void *addr) {
    struct mem_hunk *hunk = 0;
    size_t i = 0;

    /* find a hunk our frame might belong to (avoids too much fragmentation) */
    FOREACH_HUNK(i, q) {
        hunk = q->buffer+i;
        if (hunk->addr + hunk->size == (page_frame_t *) addr) {
            hunk->size++;
            return 0;
        }
        if (hunk->addr - 1 == (page_frame_t *) addr) {
            hunk->size++;
            hunk->addr--;
            return 0;
        }
    }
    /* our hunk is solitary (maybe our queue was empty?) */
    return push(q, (page_frame_t *) addr, 1);
}

size_t
pmem_available_memory(struct mem_queue *q) {
    size_t sum = 0;
    size_t i = 0;

    FOREACH_HUNK(i, q) sum += q->buffer[i].size;
    return sum * PAGE_SIZE;
}

