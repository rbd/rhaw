[cpu 686]
extern kernel_main, tty_puts
extern gdt_pointer, idt_pointer, idt_init
extern pic_initialize
extern pit_initialize

global start:function
global panic:function

section .multiboot
align 4
    ;; The Multiboot v1 header
    ;; source: https://www.gnu.org/software/grub/manual/multiboot/multiboot.html#Header-layout

    ;; magic number
    magic equ 0x1BADB002

    ;; flags:
    ;; 0 | align on 4KB page boundaries
    ;; 1 | provide a memory map
    ;; 2 | provide video mode information
    flags equ 0110b

    ;; checksum:
    ;; unsigned number such that magic + flags + checksum = 0
    checksum equ -(magic + flags)

    ;; 0 	u32 	magic 	required
    ;; 4 	u32 	flags 	required
    ;; 8 	u32 	checksum 	required
    dd magic
    dd flags
    dd checksum

section .bss
align 16 ; needed by GCC | source: https://en.wikipedia.org/wiki/X86_calling_conventions#cdecl
    resb (16*1024)
stack equ $

section .text
start:
    mov esp, stack

    ;; clear EFLAGS
    push dword 0
    popfd

    ;; save ebx (= pointer to the multiboot info)
    push ebx

    call idt_init

    ;; set GDTR & IDTR
    lgdt [gdt_pointer]
    lidt [idt_pointer]

    ;; far jump to reload our GDT (and IDT)
    jmp 0x08:.gdt_reloaded
    .gdt_reloaded:

    ;; configure PICs
    call pic_initialize

    ;; configure PIT
    call pit_initialize

    ;; pointer to multiboot info is already on the stack
    .call_c_kernel:
        call kernel_main

    ;; theorically unreachable code
    cli
    .hang:
        hlt
        jmp .hang

;; halt and catch fire
panic:
    ; don’t make a new frame, directly call tty_puts
    add esp, 4
    call tty_puts
    .hang:
        hlt
        jmp .hang

