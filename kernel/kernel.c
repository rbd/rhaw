#include <u.h>
#include <libc.h>
#include <tty.h>
#include <multiboot.h>
#include <kernel.h>
#include <memory.h>

static uint32_t system_clock;
static struct mem_queue pmem_queue;

enum {
    SYSTEM_CLOCK_FREQ = 20,
};

/**
 * Kernel main routine
 *
 * Parameters:
 * - multiboot_info_addr: pointer to the multiboot information structuro
 */
void
kernel_main(const struct multiboot_info *multiboot_info) {
    int ret = 0;

    tty_reset();
    print("Rh%xw!\r\n", 0xa);
    print("Scanning physical memory map... ");
    ret = pmem_init(&pmem_queue, multiboot_info);
    if (ret < 0) {
        panic("ERROR\r\n\tToo many hunks for our pmem allocator\r\n");
    }
    print("OK\r\n\t%u Mib available\r\n",
            pmem_available_memory(&pmem_queue) / (1024*1024));
}

/* ******************** Exception handlers ******************** */

void
divide_by_zero_handler(void) {
    panic("Divide by zero\r\n");
}

void
debug_trap_handler(void) {
    panic("Debug trap\r\n");
}


void
non_maskable_interrupt_handler(void) {
    panic("Non maskable interrupt\r\n");
}

void
breakpoint_handler(void) {
    panic("Breakpoint\r\n");
}

void
overflow_handler(void) {
    panic("Overflow\r\n");
}

void
bound_range_exceeded_handler(void) {
    panic("Bound range exceeded\r\n");
}

void
invalid_opcode_handler(void) {
    panic("Invalid opcode\r\n");
}

void
device_not_available_handler(void) {
    panic("Device not available\r\n");
}

void
double_fault_handler(uint32_t code) {
    (void)code;
    panic("Double fault\r\n");
}

void
invalid_tss_handler(uint32_t code) {
    (void)code;
    panic("Invalid TSS\r\n");
}

void
segment_not_present_handler(uint32_t code) {
    (void)code;
    panic("Segment not present\r\n");
}

void
stack_segment_fault_handler(uint32_t code) {
    (void)code;
    panic("Stack segment fault\r\n");
}

void
general_protection_fault_handler(uint32_t code) {
    (void)code;
    panic("General protection fault\r\n");
}

void
page_fault_handler(uint32_t code) {
    (void)code;
    panic("Page fault\r\n");
}

/* ******************** Hardware interrupt handlers ******************** */

/* dummy handler */
void
iq0_handler(void) {
    ++system_clock;
}
