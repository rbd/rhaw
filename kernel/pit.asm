global pit_initialize

section .text
    pit_counter equ 59659 ; 20Hz frequency — one tick every 50ms

;; Initializes the PIT’s first counter to signal every 50ms.
;; registers scrapped: eax
pit_initialize:
    ;; Send the command word to say we’re about to write the counter
    ;; (we send the least significant byte of our 2 byte value first,
    ;;  and only then can we send our most significant byte)
    mov al, 00110100b
    out 0x43, al
    mov ax, pit_counter
    out 0x40, al
    shr eax, 8
    out 0x40, al
    ret
