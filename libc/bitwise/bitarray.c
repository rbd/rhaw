/* facilities for bitmap handling */
#include <u.h>
#include <bitarray.h>

/* longuest chain of zeroes
 * return the length of the chain
 * set *p as the starting index of the chain
 *
 * If there are no chain of zeroes in x, *p’s value is undefined. */
size_t
bitwise_lcz(unsigned x, size_t *p) {
    size_t max_length = 0;
    size_t length = 0;
    size_t pos = 0;
    size_t offset = 0;
    *p = 0;

    if (!x) return sizeof(x)*8;
    /*
     * Algorithm
     * =========
     *
     * 0 |  x = 1101000010010001
     *   | ~x = 0010111101101110
     *   | offset <- 2
     *   | pos <- 2
     *   | x <- ~1011110110111000
     *   |   <-  0100001001000111
     *   | length <- 1
     *   | max_length <- 1
     *   | *p <- 2
     *   | x <- ~0111101101110000
     *   |   <-  1000010010001111
     *   | pos <- 3
     *
     * 1 |  x = 1000010010001111
     *   | ~x = 0111101101110000
     *   | offset <- 1
     *   | pos <- 4
     *   |  x <- ~1111011011100000
     *   |    <-  0000100100011111
     *   | length <- 4
     *   | max_length <- 4
     *   | *p <- 3
     *   | x <- ~0110111000000000
     *   |   <-  1001000111111111
     *   | pos <- 8
     *
     * 2 |  x = 1001000111111111
     *   | ~x = 0110111000000000
     *   | offset <- 1
     *   | pos <- 9
     *   |  x <- ~1101110000000000
     *   |    <-  0010001111111111
     *   | length <- 2
     *   | max_length and *p don’t change
     *   | x <- ~0111000000000000
     *   |   <-  1000111111111111
     *   | pos <- 11
     *
     * 3 |  x =  1000111111111111
     *   | ~x = ~0111000000000000
     *   | offset <- 1
     *   | pos <- 12
     *   |  x <- ~1110000000000000
     *   |    <-  0001111111111111
     *   | length <- 3
     *   | max_length and *p don’t change
     *   | x <- ~0000000000000000
     *   |   <-  1111111111111111
     *   | pos <- 15
     *
     * 4 |  x = 1111111111111111
     *   | ~x = 0000000000000000
     *   Stop the loop, return 4 and *p = 3.
     */
    while (~x) {
        /* skip leading ones */
        offset = bitwise_clo(x);
        pos += offset;
        /* shift x left and complete with ones */
        x = ~(~x << offset);
        /* is the current chain is greater than the previous ones? */
        length = bitwise_clz(x);
        if (length > max_length) {
            max_length = length;
            *p = pos;
        }
        /* skip the current chain */
        x = ~(~x << length);
        pos += length;
    }
    return max_length;
}
