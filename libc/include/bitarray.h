/* facilities for bitmap handling */

/* count leading zeroes */
inline unsigned
bitwise_clz(unsigned x) {
    return (unsigned) __builtin_clz(x);
}

/* count leading ones */
inline unsigned
bitwise_clo(unsigned x) {
    return (unsigned) __builtin_clz(~x);
}
