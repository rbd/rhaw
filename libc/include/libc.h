/* global libc file */

/* memory */

void *memcpy(void *dest, const void *src, size_t n);
void *memmove(void *dest, const void *src, size_t n);

/* string management */
size_t strlen(const char *s);
int strcmp(const char *s, const char *s2);

/* ASCII */
int isprint(uint8_t c);

/* print */
int itoa(char *buf, size_t nbuf, unsigned value, unsigned char base);

size_t sprint(char *buf, size_t nbuf, const char* fmt, ...);
size_t print(const char *fmt, ...);

size_t vsprint(char *buf, size_t nbuf, const char* fmt, va_list ap);
size_t vprint(const char *fmt, va_list ap);

/* bitwise operations */
size_t bitwise_lcz(unsigned x, size_t *p);
