#include <u.h>

size_t
strlen(const char *s) {
    size_t n = 0;
    while (*s++) n++;
    return n;
}

int
strcmp(const char *s1, const char *s2) {
    while (*s1 == *s2 && *s1) s1++, s2++;
    return *(unsigned char *)s1 - *(unsigned char *)s2;
}
