#include <u.h>

void *
memcpy(void *restrict dest, const void *restrict src, size_t n) {
    unsigned char *d = dest;
    const unsigned char *s = src;

    /* nothing to do */
    if (dest == src || !n) return dest;

    for (; n > 0; --n) *d++ = *s++;
    return dest;
}

