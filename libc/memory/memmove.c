#include <u.h>
#include <libc.h>

static unsigned abs(int);

void *
memmove(void *dest, const void *src, size_t n) {
    unsigned char *d = dest;
    const unsigned char *s = src;

    /* nothing to do */
    if (dest == src || !n) return dest;

    /* pointers don’t, in fact, overlap */
    if (abs((uintptr_t)d - (uintptr_t)s) > n) {
        return memcpy(d, s, n);
    }

    if ((uintptr_t)d < (uintptr_t)s) {
        for (; n > 0; --n) *d++ = *s++;
    } else {
        while (n-- > 0) d[n] = s[n];
    }
    return dest;
}

static unsigned
abs(int x) {
    return x > 0 ? x : -x;
}
