#include <u.h>

static void strreverse(char *, char *);

/* write in BUF the representation of VALUE in BASE as a nul-terminated string.
 * return the length of this representation (excluding the nul character). */
/* TODO: handle negative numbers */
int
itoa(char *buf, size_t nbuf, unsigned value, unsigned char base) {
    static char digits[] = "0123456789abcdef";
    size_t n = 0;

    /* error handling */
    if (base < 2 || base > 16) return -1;
    if (!nbuf) return 0;

    do {
        buf[n] = digits[value % base];
        value /= base;
        ++n;
    } while (n < nbuf && value);

    if (n == nbuf) return nbuf;
    strreverse(buf, buf + n - 1);
    buf[n] = 0;
    return n;
}

/* reverse a string of size nbuf */
static void
strreverse(char *head, char *tail) {
    char aux;

    while (head < tail) {
        aux = *head;
        *head++ = *tail;
        *tail-- = aux;
    }
}
