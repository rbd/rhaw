#include <u.h>
#include <libc.h>
#include <tty.h>

#define LEN(x) (sizeof(x)/sizeof(*x))

static char buf[512];

size_t
print(const char *fmt, ...) {
    va_list ap;
    size_t n = 0;

    va_start(ap, fmt);
    n = vprint(fmt, ap);
    va_end(ap);

    return n;
}

/* TODO: handle output that overflows 511 bytes */
size_t
vprint(const char *fmt, va_list ap) {
    size_t n;

    n = vsprint(buf, LEN(buf), fmt, ap);
    if (n == LEN(buf)) { /* buffer is too long; for now, truncate */
        buf[LEN(buf)-2] = '|';
        buf[LEN(buf)-1] = 0;
    }
    tty_puts(buf);
    return n;
}
