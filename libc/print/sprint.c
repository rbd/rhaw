#include <u.h>
#include <libc.h>

enum state {
    NORMAL,
    CONTROL,
};

size_t
sprint(char *buf, size_t nbuf, const char* fmt, ...) {
    va_list ap;
    size_t n = 0;

    va_start(ap, fmt);
    n = vsprint(buf, nbuf, fmt, ap);
    va_end(ap);

    return n;
}

size_t
vsprint(char *restrict buf, size_t nbuf, const char *restrict fmt, va_list ap) {
    enum state state = NORMAL;
    size_t n = 0;
    size_t len = 0;
    const char *str;

    while (n < nbuf && *fmt) {
        if (state == NORMAL) {
            switch (*fmt) {
                case '%':
                    state = CONTROL;
                    break;
                default:
                    buf[n++] = *fmt;
            }
        } else if (state == CONTROL) {
            state = NORMAL;
            switch (*fmt) {
                /* percent */
                case '%':
                    buf[n++] = '%';
                    break;
                /* unsigned */
                case 'o':
                    n += (size_t) itoa(buf+n, nbuf-n, va_arg(ap, unsigned), 8);
                    break;
                case 'u':
                    n += (size_t) itoa(buf+n, nbuf-n, va_arg(ap, unsigned), 10);
                    break;
                case 'x':
                case 'X': /* fallthrough */
                    n += (size_t) itoa(buf+n, nbuf-n, va_arg(ap, unsigned), 16);
                    break;
                /* string */
                case 's':
                    str = va_arg(ap, const char *);
                    len = strlen(str);
                    memcpy(buf+n, str, len);
                    n += len;
                default:
                    break;
                    /* not implemented, skip it */
            }
        }
        ++fmt;
    }
    if (n < nbuf) buf[n] = 0;
    return n;
}
