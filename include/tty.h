/**
 * Clear screen and reset cursor.
 */
void tty_reset(void);

/**
 * Print char and increment cursor.
 */
void tty_putc(uint8_t c);

/**
 * Print string, wrapping lines.
 */
void tty_puts(const char *s);
