enum {
    PAGE_SIZE = 4096,
};

typedef char page_frame_t[PAGE_SIZE];

struct mem_hunk {
    page_frame_t *addr; /* PAGE_SIZE-aligned pointer */
    size_t size;
};

/* Circular buffer implementation of a queue.
 *
 * +------------------------------+
 * |            XXXXXXXXXXX       |
 * |------------^---------^-------|
 * 0          first      last    max
 */
struct mem_queue {
    size_t first; /* first element */
    size_t length; /* number of elements */
    /* sized such that one struct fits in one page */
    struct mem_hunk buffer[PAGE_SIZE/sizeof(struct mem_hunk) - 2*sizeof(size_t)];
};

int pmem_init(struct mem_queue *queue, const struct multiboot_info *info);
void *pmem_alloc(struct mem_queue *queue);
int pmem_free(struct mem_queue *queue, void *addr);
size_t pmem_available_memory(struct mem_queue *queue);
