struct multiboot_info {
    uint32_t flags;
    /* amount of lower memory */
    size_t mem_lower;
    /* amount of higher memory */
    size_t mem_higher;

    uint32_t boot_device;
    const char *cmdline;
    size_t mods_count;
    void *mods_addr;
    uint32_t syms[4];

    /* length of the memory map */
    size_t mmap_length;
    /* address of the memory map */
    void *mmap_addr;
    size_t drives_length;
    void *drives_addr;
    uint32_t config_table;
    uint32_t boot_loader_name;
    uint32_t apm_table;
} __attribute__((packed));

enum multiboot_info_flags {
    /* are the mem_lower and mem_higher valid? */
    MB_INFO_VALID_MEMFIELDS = 1 << 0,

    /* we don’t care about other flags */
    MB_INFO_VALID_MMAP = 1 << 6,
};

/* describes a memory chunk */
struct multiboot_mmap_entry {
    /* size of the entry (size is *not* included) */
    size_t size;
    /* base address of the memory chunk */
    uint64_t base_addr;
    /* size of the memory chunk */
    uint64_t length;
    /* is this chunk available or not? */
    uint64_t type;
} __attribute__((packed));

enum multiboot_mmap_entry_type {
    MB_MMAP_ENTRY_AVAILABLE = 1,
};


/* iterate mmap over info->mmap_addr
 * - mmap must be an identifier for a pointer to struct multiboot_mmap_entry;
 * - info must be an identifier for a pointer to struct multiboot_info. */
#define FOREACH_MMAP(mmap, info) \
    for (mmap = info->mmap_addr; \
            (uintptr_t)mmap < (uintptr_t)info->mmap_addr + info->mmap_length; \
            mmap = (struct multiboot_mmap_entry *) \
            ((uintptr_t)mmap + mmap->size + sizeof(mmap->size)))
