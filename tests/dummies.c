/* Sometimes, dummy functions are needed to compile our tests */

#include <stdint.h>

void
tty_reset(void) {
}

void
tty_putc(uint8_t c) {
    (void)c;
}

void
tty_puts(const char *s) {
    (void)s;
}
