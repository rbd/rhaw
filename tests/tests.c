#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <testing.h>

void test_itoa(void);
void test_sprint(void);
void test_bitwise_lcz(void);

int
main(void) {
    ISEQ(1,1);
    test_itoa();
    test_sprint();
    test_bitwise_lcz();
    testing_sumup();
    return 0;
}
