/* testing.h — minimal unit testing library */
/* *** dependencies ***
   #include <stdio.h>
   #include <string.h>
   */

#define COLOR_NORM "[0;37;40m"
#define COLOR_PASS "[2;37;40m"
#define COLOR_FAIL "[1;31;40m"
#define COLOR_PASS_SUMUP "[1;32;40m"
#define COLOR_FAIL_SUMUP "[1;31;40m"

static size_t passed;
static size_t failed;

static void
testing_assert(int val, const char *msg,
               const char *file, int line) {
    if (val) {
        ++passed;
        fputs(COLOR_PASS "." COLOR_NORM, stdout);
    } else {
        ++failed;
        printf(COLOR_FAIL "\nFAIL" COLOR_NORM "\t%s:%d\t%s\n",
               file, line, msg);
    }
}

#define ISEQ(val, expr) testing_assert((val) == (expr),        \
                                       # val " != " # expr,    \
                                       __FILE__, __LINE__)

#define ISSE(val, expr) testing_assert(strcmp((val), (expr)) == 0,    \
                                       # val " != " # expr,           \
                                       __FILE__, __LINE__)

static void
testing_sumup(void) {
    printf(COLOR_NORM "\n---------- "
           COLOR_PASS_SUMUP "PASSED: %zu"
           COLOR_FAIL_SUMUP "\tFAILED: %zu"
           COLOR_NORM " ----------\n",
           passed, failed);
}
