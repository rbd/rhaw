#include <u.h>
#include <stdio.h>
#include <libc.h>
#include <testing.h>

void
test_bitwise_lcz(void) {
    size_t p = 0;
    size_t l = 0;

    l = bitwise_lcz(0U, &p);
    ISEQ(l, 32);
    ISEQ(p, 0);

    l = bitwise_lcz(1U, &p);
    ISEQ(l, 31);
    ISEQ(p, 0);

    l = bitwise_lcz(0b011111111111111111111111111111111, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    /* well… I took random numbers and counted the longuest chain
     * myself */
    l = bitwise_lcz(0b000000001001110010111110110001101, &p);
    ISEQ(l, 7);
    ISEQ(p, 0);

    l = bitwise_lcz(0b01101011111110001000110000110001, &p);
    ISEQ(l, 4);
    ISEQ(p, 22);

    l = bitwise_lcz(0b00011100110010100111100101111010, &p);
    ISEQ(l, 3);
    ISEQ(p, 0);

    l = bitwise_lcz(0b11101010011011011010010010101110, &p);
    ISEQ(l, 2);
    ISEQ(p, 7);

    l = bitwise_lcz(0b10100011110001111000100000000111, &p);
    ISEQ(l, 8);
    ISEQ(p, 21);

    l = bitwise_lcz(0b11001010110011100001100101101001, &p);
    ISEQ(l, 4);
    ISEQ(p, 15);

    /*l = bitwise_lcz(0b11100000111000001101010010101101, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b11110101101000010100101110101011, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b10000000111100000000100110001000, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b10100111110111101001111101001100, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b11001100010001010000110010111010, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b00001001001001000110011010001111, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b01011100011111011100001110000000, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b11011001011000001000100111000101, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b00110110010000001010110001001100, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b11101111000110100010111001101101, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b10101110011011011001010000100110, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b10101101110000011001011001011011, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b01100110000100111011101001000110, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b11000001111110110100000111000010, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b10111101100110110000111011001101, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b10111110001111011110110111111100, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b01111001100010011100100011101110, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b01100100011010001111110101101110, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b01101100000011011111000000110010, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b10100111110011010110011000110100, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b00101100100000100110110110001011, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b00101011110100101110010000010010, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b01001101010010100010110110111110, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);

    l = bitwise_lcz(0b10110100101111110110111110100111, &p);
    ISEQ(l, 0);
    ISEQ(p, 0);*/
}

