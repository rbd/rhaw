#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <libc.h>
#include <testing.h>

#define LEN(x) (sizeof(x)/(sizeof(*x)))

static char buf[512];

void
test_itoa(void) {
    int base = 0;
    int ret = 0;

    /* 0 in all bases */
    for (base = 2; base <= 16; ++base) {
    ret = itoa(buf, LEN(buf), 0, base);
        ISSE("0", buf);
        ISEQ(1, ret);
    }
    /* 1 in all bases (all ur bases belong to us) */
    for (base = 2; base <= 16; ++base) {
        ret = itoa(buf, LEN(buf), 1, base);
        ISSE("1", buf);
        ISEQ(1, ret);
    }

    ret = itoa(buf, LEN(buf), 123, 10);
    ISSE("123", buf);
    ISEQ(3, ret);
    ret = itoa(buf, LEN(buf), 255, 16);
    ISSE("ff", buf);
    ISEQ(2, ret);

    /* buffer is too short */
    ret = itoa(buf, 3, 12345, 10);
    ISEQ(ret, 3);
}

void
test_sprint(void) {
    int ret = 0;

    /* octal */
    ret = sprint(buf, LEN(buf), "%o", 0123);
    ISSE("123", buf);
    ISEQ(3, ret);

    /* decimal */
    ret = sprint(buf, LEN(buf), "%u", 4321);
    ISSE("4321", buf);
    ISEQ(4, ret);

    /* hexadecimal (both notations) */
    ret = sprint(buf, LEN(buf), "%x %X", 0xabc, 0xdef);
    ISSE("abc def", buf);
    ISEQ(7, ret);

    /* control at the end */
    ret = sprint(buf, LEN(buf), "abc %u", 938);
    ISSE("abc 938", buf);
    ISEQ(7, ret);

    /* control at the beginning */
    ret = sprint(buf, LEN(buf), "%u def", 8);
    ISSE("8 def", buf);
    ISEQ(5, ret);

    /* control in the middle */
    ret = sprint(buf, LEN(buf), "foo %u bar", 189);
    ISSE("foo 189 bar", buf);
    ISEQ(11, ret);

    /* as much as we can */
    ret = sprint(buf, LEN(buf), "%u alice %x bob %o", 765, 0xbeef, 065);
    ISSE("765 alice beef bob 65", buf);
    ISEQ(21, ret);

    /* buffer too short */
    ret = sprint(buf, 6, "foo %x\n", 0xcafe);
    ISEQ(ret, 6);
}
