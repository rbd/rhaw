org 0x7E00
bits 16

jmp start

gdt:
    .null:
        dq 0

    .code:
        dw 0xFFFF
        dw 0
        db 0
        db 10011010b
        db 11001111b
        db 0

    .data:
        dw 0xFFFF
        dw 0
        db 0
        db 10010010b
        db 11001111b
        db 0
gdt_end:

gdt_descriptor:
    dw gdt_end - gdt
    dd gdt

start:
    mov si, msg_init
    call print_string

;; Ensure the A20 line is activated.
;; If activation is needed, it is done by the BIOS.
;; Theorically, we should try to activate A20 by other means (keyboard
;; controller and such), but since this bootloader is just an exercice,
;; let’s allow ourselves to forget that.
a20_activation:
    .check:
        call test_a20
        jnc .done

    .activate:
        mov si, msg_a20_disabled
        call print_string
        mov ax, 0x2401
        int 0x15

    .check_again:
        call test_a20
        jnc .done

    .abort:
        mov si, msg_a20_abort
        call print_string
        jmp hang

    .done:
        mov si, msg_a20_enabled
        call print_string

    jmp protected_mode

;; tests whether the A20 gate is opened or closed, by checking if memory is
;; wrapping at 1Mib.
;;
;; in: nothing
;; out: carry flag is set if A20 is disabled
;; registers altered: ax, bx, fs
;; memory altered: 0x7DFE and 0x7DFF (the bootloader magic numbers)
;; note: needs ds to be nul.
test_a20:
    mov ax, 0xFFFF ; prepare an extra segment
    mov fs, ax
    mov ax, [0x7DFE]
    cmp ax, [fs:0x7E0E] ; 0x7E0E = 0x107DFE - FFFF0
    jne .enabled
    ;; maybe our two bytes are equal by pure luck.
    ;; let’s change our value in the lower’s 64Kio and see if it’s been
    ;; echoed in the upper 64Kio.
    mov word [0x7DFE], 0x1234
    cmp word [fs:0x7E0E], 0x1234
    jne .enabled
    stc
    ret
    .enabled:
        clc
        ret

%include 'inc/biosio.asm'

msg_init db `Stage 2\r\n`, 0
msg_a20_enabled db `A20 activated\r\n`, 0
msg_a20_disabled db `Activating A20\r\n`, 0
msg_a20_abort db `Abort: could not activate A20\r\n`, 0


protected_mode:
    cli ; TODO: disable Non-Maskable Interrupts
    lgdt [gdt_descriptor]
    mov eax, cr0
    or eax, 1
    mov cr0, eax
    jmp 0x08:.done

    bits 32
    .done:
        ;; init all segments, again
        mov eax, 0x10
        mov ds, eax
        mov es, eax
        mov ss, eax
        mov esp, 0x7C00 ; same stack as before

hang:
    jmp hang


times 512 - ($-$$) db 0
