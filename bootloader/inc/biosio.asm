;; reset a drive
;; in: drive number (ax), number of retry (cx)
;; out: status (carry flag)
;; registers altered: cx, dx
;; stack usage: 2w
reset_drive:
    .retry:
        push ax
        push cx ; BIOS sometimes trash cx
        mov dx, ax ; drive number
        xor ax, ax ; reset is int 0x13/0x00
        int 0x13
        pop cx
        pop ax
        jnc .done
        dec cx
        jns .retry
    .done:
        ret ; carry flag set => could not reset drive

;; read sectors from a drive
;; in: drive number (ax), number of retry (cx), dest (di)
;; todo: number of sectors (stack, 1 word), offset (stack, 1 word)
;; out: status (carry flag)
;; registers altered: ax, bx, cx, dx, di
;; stack usage: 3w
read_sectors:
    .retry:
        push ax
        push cx
        push di
        mov dx, ax ; drive number
        mov ax, 1 ; number of sectors
        mov bx, di ; dest
        mov cx, 0x0002 ; cylinder 0 — starting at sector 2
        ; head: dh is 0 already
        mov ah, 0x02 ; load sectors is 0x13/0x02
        int 0x13
        pop di
        pop cx
        pop ax
        jnc .done
        dec cx
        jns .retry
    .done:
        ret

;; print_string
;; in: source (si)
;; out: none
;; registers altered: ax, bx
;; stack usage: 0
;; note: direction flag MUST be cleared prior to calling this procedure
print_string:
    xor bx, bx ; page — attrs
    .loop:
        lodsb
        test al, al
        jz .done
        mov ah, 0x0E ; print tty character is 0x10/0x0E
        int 0x10
        jmp .loop
    .done:
        ret
