org 0x7C00
bits 16

start:
    ;; init all segments as 0
    ;; stack grows down from 7BFFh
    xor ax, ax
    mov ds, ax
    mov es, ax
    mov ss, ax
    mov sp, 0x7C00

    mov [drive_number], dl

    cld
    mov si, msg_init
    call print_string

    mov ax, [drive_number]
    mov cx, 5
    call reset_drive
    jc abort
    mov si, msg_reset_floppy_drive
    call print_string

    mov ax, [drive_number]
    mov cx, 5
    mov di, 0x7E00
    call read_sectors
    jc abort
    mov si, msg_load_first_sectors
    call print_string

    .stage_2:
        jmp 0x0000:0x7E00

abort:
    mov si, msg_abort
    call print_string
    .halt: jmp .halt

%include 'inc/biosio.asm'

msg_init: db `Stage 1\r\n`, 0
msg_reset_floppy_drive: db `Floppy drive reset\r\n`, 0
msg_load_first_sectors: db `Sector 1 loaded\r\n`, 0
msg_retry: db `Retry\r\n`, 0
msg_abort: db `Tried 5 times, aborting\r\n`, 0

drive_number: dw 0

times 510 - ($-$$) db 0
dw 0xAA55
